<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     */
    public function index()
    {
        // dump(array("anas" => 2));
        return $this->render('main/main.html.twig',["form_name"=>"Dummy Form"]);
    }

    /**
     * @Route("/api/main/{page<\d+>}", name="main_show")
     * 
     */
    public function show(int $page){
        return new Response('<h1>i m page</h1>');
    }

    /**
     * @Route("/api/main/{slug}", name="main_list")
     * 
     */
    public function list(string $slug){
        
        dump(array('slug'=>$slug));
        return new Response('<h1>i m slug</h1>');
    }
}
